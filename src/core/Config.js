const config = {
    APP_URL: 'http://localhost:3000',
    NODE_API_URL: 'http://localhost:3002/api/',
};

export default config;