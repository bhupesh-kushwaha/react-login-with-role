import React from 'react'
import { Route, Redirect } from 'react-router-dom'
import Header from './components/layout/Header';
import Footer from './components/layout/Footer';

const PrivateRoute = ({ component: Component, ...rest }) => (

    <Route {...rest} render={(props) => (
    	localStorage.getItem('user')
    	?
        <div className="page">
            <Header />

            <div className="container-fluid">

            <Component {...props} />

            </div>
            
            <Footer />
        </div>
        : <Redirect to={{ pathname: '/login', state: { from: props.location } }} />
    )}/>
);

export default PrivateRoute;
