import React, {Component} from 'react';
import PrivateRoute from './PrivateRoute';
import { Switch, Route } from 'react-router-dom';
import Login from '../login';
import Dashboard from '../dashboard'

export default class Root extends Component{
    render(){

        return(
            <Switch>
                <Route exact path="/login" component={Login} />
                <PrivateRoute exact path="/" component={Dashboard} />
            </Switch>
        );
    }
}