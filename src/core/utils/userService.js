import Config from '../Config';

export const userService = {
    login,
    logout,
    getAll,
    getById
};

function login(email, password) {

    const options = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ email, password })
    };

    return fetch( Config.NODE_API_URL + 'login', options)
        .then(handleErrors)
        .then(user => {
            if (user && user.status === 200) {
                if( user.response.length > 0 ) {
                    localStorage.setItem('user', JSON.stringify(user.response));
                }
            }
            
            return user;
        });
}

function logout() {
    localStorage.removeItem('user');
}

function getAll() {
    const options = {
        method: 'GET'
    };

    return fetch(Config.NODE_API_URL + 'user', options).then(handleErrors);
}

function getById(ID) {
    
    const options = {
        method: 'GET'
    };

    return fetch(Config.NODE_API_URL + 'user/'+ ID, options).then(handleErrors);
}

function handleErrors(response) {
    return response.text().then(text => {

        const data = text && JSON.parse(text);

        if (!response.ok) {

            if (response.status === 401) {
                logout();

                window.location.reload(true);
            }

            const error = (data && data.message) || response.statusText;

            return Promise.reject(error);
        }

        return data;
    });
}