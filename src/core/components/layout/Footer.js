import React from 'react';

export default class Footer extends React.Component {
    render(){
        return(
        	<div>
           <footer className="container-fluid bg-4 text-center">
			  Made By <b className="text-primary"> Bhupesh Kushwaha</b> 
			</footer>
			</div>
        );
    }
}