import React, {Component} from 'react';

export default class NotFound extends Component{

    render(){

        return(
            <div className="container">
                <div className="row">
                    <div className="col-12">
                        <h3>Page Not Found!</h3>
                    </div>
                </div>
            </div>
        );
    }
}