import React from 'react';
import { Link } from 'react-router-dom';

export default class Header extends React.Component {
    render(){
        return(
            <div className="header">
            	<nav className="navbar navbar-expand-sm bg-dark navbar-dark">

				  <a className="navbar-brand" href="#0">React Login App</a>
				  
				  <ul className="navbar-nav">
				    <li className="nav-item">

				      <Link className="nav-link"  to="/login">Logout</Link>

				    </li>
				  </ul>
				</nav>
            </div>
        );
    }
}