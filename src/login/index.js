import React from 'react';
import { userService } from '../core/utils/userService';
import './login.css';

export default class Login extends React.Component {
	constructor(props) {
        super(props);

        userService.logout();

        this.state = {
            email: '',
            password: '',
            submitted: false,
            loading: false,
            error: ''
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(e) {
        const { name, value } = e.target;

        this.setState({ [name]: value });
    }

    handleSubmit(e) {
        e.preventDefault();

        this.setState({ submitted: true });

        const { email, password } = this.state;

        if (!(email && password)) {
            return;
        }

        this.setState({ loading: true });

        userService.login(email, password)
            .then( user => {
            		
            		if( user.status === 200 ) {
            			if( user.response.length > 0 ) {
                    		const { from } = this.props.location.state || { from: { pathname: "/" } };
                    	
                    		this.props.history.push(from);
                    	}
                    	else {
                    		this.setState({ error: 'Records not found', loading: false })
                    	}
                	}
                	else{
                		this.setState({ error: user.error[0].msg, loading: false })
                	}

                }
            )
            .catch(
            	error => this.setState({ error, loading: false })
            );
    }

    render(){
    	const { email, password, submitted, loading, error } = this.state;

        return(
			<div className="main">

				<div className="container">
					<center>
						<div className="middle">
							<div id="login">

								<form name="form" onSubmit={this.handleSubmit}>

									{ error && <div className={'alert alert-danger'}>{error}</div> }
									
									<fieldset className="clearfix">

									<p className={'form-group' + (submitted && !email ? ' has-error' : '')}>
										<span className="fa fa-user"></span>
										
										<input 
											type="email" 
											className="form-control" 
											name="email" 
											value={email} 
											onChange={this.handleChange} 
											placeholder="Email"
											required
										/>
				                        { submitted && !email && <span className="help-block">email is required</span> }
									</p> 
									<p className={'form-group' + (submitted && !password ? ' has-error' : '')}>
										<span className="fa fa-lock"></span>

										<input 
											type="password" 
											className="form-control" 
											name="password" 
											value={password} 
											onChange={this.handleChange} 
											pattern="^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$"
											placeholder="Password"
											required
										/>
				                        { submitted && !password && <div className="help-block">Password is required</div> }
									</p>

									<div className="form-group">
										<button className="btn btn-primary" disabled={loading}>Login</button>
				                        {
				                        	loading &&
				                            <img src="data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA==" alt="loader"/>
				                        }
									</div>

									</fieldset>


									<div className="clearfix"></div>
								</form>

								<div className="clearfix"></div>

							</div>

							<div className="logo">
								Login
								<div className="clearfix"></div>
							</div>

						</div>
					</center>
				</div>

			</div>
        );
    }
}