import React from 'react';
import Profile from '../images/profile.jpg';

export default class userDetails extends React.Component {
    render(){
        return(
        <div className="row">    
        <div className="col-12">

            <div className="card user-details w-100">

              <img className="w-100 h-50" src={Profile} alt={this.props.user.full_name} />
              
              <br />
              
              <h3><b>{this.props.user.full_name}</b></h3>
              
              <p className="title">{this.props.user.mob_no}</p>
              
              <p>{this.props.user.email}</p>

              <p>{this.props.user.role}</p>
              
              <div className="m-5">
                <a href="#0" target="_blank"><i className="fa fa-linkedin"></i></a>
                <a href="#0" target="_blank"><i className="fa fa-facebook"></i></a>
              </div>
              
              <p><a href="#0" className="button">Profile</a></p>
            </div>
            </div>
        </div>    
        );
    }
}