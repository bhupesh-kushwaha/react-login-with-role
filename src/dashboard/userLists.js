import React from 'react';

export default class userLists extends React.Component {
    render(){
        return(  
            <div className="row">
                <div className="col-md-12">
                    <table className="table table-hover">

                        <thead>
                          <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>View</th>
                          </tr>
                        </thead>

                        <tbody>

                            { this.props.users.map((user, index) => 
                            <tr key={ user.id }> 
                                <td>{user.full_name}</td>
                                <td>{user.email}</td>
                                <td>{user.phone}</td>
                                <td> <button 
                                        name={user.id} 
                                        onClick={this.props.handleCallBackId}
                                    >View</button></td>
                            </tr>
                            )}
                                              
                        </tbody>

                      </table>
                </div> 
            </div> 
        );
    }
}