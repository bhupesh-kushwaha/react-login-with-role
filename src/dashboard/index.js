import React from 'react';
import { userService } from '../core/utils/userService';
import UserDetails from './userDetails';
import UserLists from './userLists';
import { withRouter } from 'react-router-dom'

class Dasboard extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            current_user: {},
            users: [],
            user_details: {},
            user_id : null,
            role: null,
        };

        this.handleCallBackId = this.handleCallBackId.bind(this);   
    }

    componentDidMount() {

        var localStorageUser = JSON.parse( localStorage.getItem('user') );

        this.setState({ 
            current_user: localStorageUser[0],
            role:  localStorageUser[0]['role'] 
        });

        if( localStorageUser[0]['role']  === 'admin' ) {            
        	this.setState({ 
        		users: { loading: true }
        	});

        	userService.getAll().then(
        		users => this.setState({ users: users.response })
        	);
    	}

        this.setSubComponent();
    }



    handleCallBackId(event){
        let id = event.target.name;
        console.log('aa', id);

        this.setState({user_id: id});

        userService.getById(id).then(
            user => this.setState({ user_details: user.response[0] })
        );
    }

    setSubComponent = () => {

		const { current_user, users, user_details, user_id, role } = this.state;

        console.log(user_details, 'user_details');

		if( role === 'admin' ) {            
	    	if( user_id === null && users.length > 0) {                
	    		return (
	    			<UserLists users={users} handleCallBackId={this.handleCallBackId} />
	    		)
	    	}
	    	else {                
	    		return (
	    			<UserDetails user={user_details}/>
	    		)
	    	}
	    }
	    else {            
	    	return (
	    		<UserDetails user={current_user}/>
	    	)
	    }
    }



    render() {
        const { current_user, users, user_id } = this.state;
        

        return (
            <div className="col-md-6 col-md-offset-3">
                <h1>Hi {current_user.full_name}! 
                {
                    (user_id !== null)?
                <button
                    className="btn btn-danger float-right"
                    onClick={() => {this.setState({
                        user_id: null,
                        user_details: {}
                    })}}
                >
                Back
              </button>:""
            }
                </h1>
             
                {users.loading && <em>Loading users...</em>}

                {this.setSubComponent()}

            </div>
        );
    }
}

export default withRouter(Dasboard);