## React App | Login

### Installation and Configuration

**1. Enter git clone and the repository URL at your command line::** 
~~~
git clone https://bhupesh19921@bitbucket.org/bhupesh-kushwaha/react-login-with-role.git
~~~

**2. Goto react-login-with-role directory :** 
~~~
npm install
~~~

**3. Change config file from `src/core/Config.js` :** 
~~~
APP_URL: 'http://localhost:3000',

NODE_API_URL: 'http://localhost:3001/api/',
~~~

**4. Finally, Start your server:**
~~~
npm start
~~~